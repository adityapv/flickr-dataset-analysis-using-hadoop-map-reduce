package com.cloud.ass1.task1;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.charset.CharsetEncoder;

import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

public class PhotoPlaceIdMapper extends Mapper<Object, Text, Text, Text> {
	
	Text photoCount = new Text(), locality = new Text();
	
	static CharsetEncoder asciiEncoder = Charset.forName("US-ASCII").newEncoder();
	
	public void map (Object key, Text value, Context localityPhotoCountContext) throws IOException, InterruptedException {
		
		//System.out.println("\n"+"\n"+"\n"+value.toString());
		String[] joinArray = value.toString().split("ptag");
		
		String[] placeIdlocalityString = joinArray[0].split("\t");
		//System.out.println("\n"+"\n"+"\n"+joinArray[0]);
		
		locality.set(placeIdlocalityString[2]);
		
		//String[] photoStringArray = joinArray[1].split(",");
		
		int photoCounter = joinArray.length-1 ;
		
		photoCount.set(String.valueOf(photoCounter));
		
		localityPhotoCountContext.write(locality, photoCount);
		
	}

}
