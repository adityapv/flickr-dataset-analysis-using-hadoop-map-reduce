package com.cloud.ass1.task1;

import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Partitioner;

public class GlobalLocalityPartitioner extends Partitioner<Text, Text>{
	
	@Override
	public int getPartition(Text key, Text Value, int numPartitions){
		
		String keyArray = key.toString();
		String result = keyArray.replaceAll("[-+.^:,]","");
		String res = result.toLowerCase();
		//System.out.println("\n"+"\n"+"\n"+"\n"+res+"\n"+"\n"+"\n"+"PART-----------");
		return Math.abs(res.toString().hashCode() * 127) % numPartitions;
	}

}
