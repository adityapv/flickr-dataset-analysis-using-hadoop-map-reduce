package com.cloud.ass1.task1;

import java.io.IOException;

import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.Reducer.Context;

public class PhotoPlaceIdReducer extends Reducer<Text, Text, Text, Text>{
	
	public void reduce (Text key, Iterable<Text> values, Context localityPhotoContext) throws IOException, InterruptedException {
		
		Text locality = key, photoCount = new Text();
		String photoCountString = new String();
		int photoCounter = 0;
		for (Text text : values){
			
			photoCountString = text.toString();
			photoCounter += Integer.parseInt(photoCountString);
		}
		
		photoCount.set(String.valueOf(photoCounter));
		
		localityPhotoContext.write(locality,photoCount);
		
	}
}
