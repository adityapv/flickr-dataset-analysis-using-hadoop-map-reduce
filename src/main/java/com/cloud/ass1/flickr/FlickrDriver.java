package com.cloud.ass1.flickr;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.MultipleInputs;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;
import org.apache.hadoop.util.GenericOptionsParser;

import com.cloud.ass1.comparators.IntTextPair;
import com.cloud.ass1.comparators.IntTextPair1;
import com.cloud.ass1.comparators.JoinGroupComparator;
import com.cloud.ass1.comparators.JoinGroupComparator1;
import com.cloud.ass1.comparators.ValueSortComparator;
//import com.cloud.ass1.comparators.JoinGroupComparator;
//import com.cloud.ass1.comparators.PhotoCountComparator;
import com.cloud.ass1.preTask.*;
import com.cloud.ass1.task1.*;
import com.cloud.ass1.task2.*;
import com.cloud.ass1.task3.LocationOtherMapper;
import com.cloud.ass1.task3.LocationPhotoCountMapper;
import com.cloud.ass1.task3.LocationPhotoDetailsReducer;
import com.cloud.ass1.task3.TagCountCombiner;
import com.cloud.ass1.task3.TagCountMapper;
import com.cloud.ass1.task3.TagCountReducer;
import com.cloud.ass1.task3.TagSortMapper;
import com.cloud.ass1.task3.TagSortReducer;
import com.cloud.ass1.task3.Task3Comparator;
import com.cloud.ass1.task3.Task3Pair;
import com.cloud.ass1.task3.Task3Partitioner;


/**
 * Hello world!
 *
 */
public class FlickrDriver {

    public static void main( String[] args ) throws Exception {
    	
    	// Creating configuration for MR job
    	Configuration conf = new Configuration();
    	
    	// String to process hadoop args
    	String[] jobArgs = new GenericOptionsParser(conf, args).getRemainingArgs();
    	
    	if (jobArgs.length != 3) {
			System.err.println("Usage: FlickrDriver <in> <in> <out>");
			System.exit(2);
		}
    	
    	// Setting IO Path variables from Args
    	Path photoPath = new Path(jobArgs[0]+"/*");
    	Path placePath = new Path(jobArgs[1]);
    	
    	Path task3AInPath = new Path(jobArgs[2]+"/Task0/*");
    	Path task3BInPath = new Path(jobArgs[2]+"/Task2/*");
    	Path tagSortInPath = new Path(jobArgs[2]+"/Task3/*");
    	Path tagCountInPath = new Path(jobArgs[2]+"/Task3/*");


    	

    	
    	Path preTaskOutPath = new Path(jobArgs[2] + "/Task0");
    	Path task1OutPath = new Path(jobArgs[2] + "/Task1");
    	Path task2OutPath = new Path(jobArgs[2] + "/Task2");
    	Path task3OutPath = new Path(jobArgs[2] + "/Task3");
    	Path tagSortOutPath = new Path(jobArgs[2] + "/Task4");
    	Path tagCountOutPath = new Path(jobArgs[2] + "/Task5");

    	
    	//Path filterOutPath = new Path(jobArgs[2] + "/TaskFilter");
    	String task1InputString = jobArgs[2] + "/Task0/part-r-00*";
    	String task2InputString = jobArgs[2] + "/Task1/part-r-00*";
    	//String filterInputString = jobArgs[2] + "/Task2/part-r-00*";
    	
    	// Creating new Job to run the pretask configuration    	
    	@SuppressWarnings("deprecation")
		Job preTaskJob = new Job(conf, "Job to join required fields into a single file");
    	preTaskJob.setNumReduceTasks(6);
    	preTaskJob.setJarByClass(FlickrDriver.class);
    	
    	MultipleInputs.addInputPath(preTaskJob, photoPath, TextInputFormat.class, PhotoMapper.class);
    	MultipleInputs.addInputPath(preTaskJob, placePath, TextInputFormat.class, LocalityMapper.class);
 
    	preTaskJob.setReducerClass(FileJoinReducer.class);
    	
    	preTaskJob.setOutputKeyClass(Text.class);
    	preTaskJob.setOutputValueClass(Text.class);
    	TextOutputFormat.setOutputPath(preTaskJob, preTaskOutPath);
    	
    	preTaskJob.waitForCompletion(true);

     	// Creating new Job to run the task1 configuration
    	@SuppressWarnings("deprecation")
		Job task1Job = new Job(conf, "Job to count Photos according to Locality");
    	task1Job.setNumReduceTasks(6);
    	task1Job.setJarByClass(FlickrDriver.class);
    	
    	TextInputFormat.addInputPath(task1Job, new Path(task1InputString));    
    	
    	task1Job.setMapperClass(PhotoPlaceIdMapper.class);
     	task1Job.setReducerClass(PhotoPlaceIdReducer.class);
     	task1Job.setPartitionerClass(GlobalLocalityPartitioner.class);
    	
		task1Job.setOutputKeyClass(Text.class);
		task1Job.setOutputValueClass(Text.class);
    	TextOutputFormat.setOutputPath(task1Job, task1OutPath);
    	
		task1Job.waitForCompletion(true);
		
		// Creating new Job to run the task2 configuration 
    	@SuppressWarnings("deprecation")
		Job task2Job = new Job(conf, "Job to sort top 50 photo localities");
    	
    	task2Job.setNumReduceTasks(1);
    	task2Job.setJarByClass(FlickrDriver.class);
		
    	TextInputFormat.addInputPath(task2Job, new Path(task2InputString));    	
    	
		task2Job.setMapperClass(Top50Mapper.class);
		task2Job.setMapOutputKeyClass(IntTextPair.class);
		task2Job.setMapOutputValueClass(NullWritable.class);
		
		task2Job.setGroupingComparatorClass(JoinGroupComparator.class);
		task2Job.setPartitionerClass(Task2Partitioner.class);
 	   	
		task2Job.setReducerClass(Top50Reducer.class);
 	   	
    	
    	task2Job.setOutputKeyClass(Text.class);
		task2Job.setOutputValueClass(Text.class);
		FileOutputFormat.setOutputPath(task2Job, task2OutPath);

		task2Job.waitForCompletion(true);
		
    	// Creating new Job to run the task3 configuration    	
    	@SuppressWarnings("deprecation")
		Job task3Job = new Job(conf, "Filter tags according to location");
    	task3Job.setNumReduceTasks(6);
    	task3Job.setJarByClass(FlickrDriver.class);
    	
    	MultipleInputs.addInputPath(task3Job, task3AInPath, TextInputFormat.class, LocationOtherMapper.class);
    	MultipleInputs.addInputPath(task3Job, task3BInPath, TextInputFormat.class, LocationPhotoCountMapper.class);
 
    	task3Job.setReducerClass(LocationPhotoDetailsReducer.class);
    	
    	task3Job.setOutputKeyClass(Text.class);
    	task3Job.setOutputValueClass(Text.class);
    	TextOutputFormat.setOutputPath(task3Job, task3OutPath);	
    	
		task3Job.waitForCompletion(true);

    	
    	// Job to sort tags
		/*Job task3SortJob = new Job(conf, "Filter tags according to location");
		task3SortJob.setNumReduceTasks(6);
		task3SortJob.setJarByClass(FlickrDriver.class);
		
    	TextInputFormat.addInputPath(task3SortJob, tagSortInPath);    	

		task3SortJob.setMapperClass(TagSortMapper.class);
		//task3SortJob.setCombinerClass(TagSortReducer.class);
		task3SortJob.setMapOutputKeyClass(Task3Pair.class);
		task3SortJob.setMapOutputValueClass(Text.class);
		
		task3SortJob.setGroupingComparatorClass(Task3Comparator.class);
		task3SortJob.setPartitionerClass(Task3Partitioner.class);
		
		task3SortJob.setOutputKeyClass(Text.class);
		task3SortJob.setOutputValueClass(Text.class);
		task3SortJob.setReducerClass(TagSortReducer.class);
		
		TextOutputFormat.setOutputPath(task3SortJob, tagSortOutPath);
		//FileOutputFormat.setOutputPath(task3SortJob, tagSortOutPath);
	
		task3SortJob.waitForCompletion(true);
*/
		
		
		// Job to count frequency
		Job task3CountJob = new Job(conf, "Count tags job");
		task3CountJob.setNumReduceTasks(1);
		task3CountJob.setJarByClass(FlickrDriver.class);
		
    	TextInputFormat.addInputPath(task3CountJob, tagCountInPath);    	

    	task3CountJob.setMapperClass(TagCountMapper.class);
		//task3CountJob.setCombinerClass(TagSortReducer.class);
    	task3CountJob.setMapOutputKeyClass(Text.class);
    	task3CountJob.setMapOutputValueClass(Text.class);
		
    	//task3CountJob.setGroupingComparatorClass(Task3Comparator.class);
    	//task3CountJob.setPartitionerClass(Task3Partitioner.class);
		
    	task3CountJob.setOutputKeyClass(Text.class);
    	task3CountJob.setOutputValueClass(Text.class);
    	task3CountJob.setCombinerClass(TagCountCombiner.class);
    	task3CountJob.setReducerClass(TagCountReducer.class);

		TextOutputFormat.setOutputPath(task3CountJob, tagCountOutPath);
		
 		System.exit(task3CountJob.waitForCompletion(true) ? 0 : 1);

		
		
/*		// ///////////////////////////////////////////////// Backup
		
		Job taskFilterJob = new Job(conf, "Job to sort top 50 photo localities");
		taskFilterJob.SetMapper.class(FilterMapper.class);
		TextInputFormat.addInputPath(task2Job, new Path(filterInputString));
		TextOutputFormat.setOutputPath(taskFilterJob, filterOutPath);
    	taskFilterJob.setOutputKeyClass(Text.class);
		taskFilterJob.setOutputValueClass(Text.class);
	*/	/////////////////////////////////////////////////////
		
/*    	//TextOutputFormat.setOutputPath(task2Job, task2OutPath);
    	FileOutputFormat.setOutputPath(task2Job, task2OutPath);
		
		// SecondarySort Method TASK2
    	@SuppressWarnings("deprecation")
		Job task2Job = new Job(conf, "Job to sort top 50 photo localities");
    	
    	task2Job.setNumReduceTasks(6);
    	task2Job.setJarByClass(FlickrDriver.class);
		
    	TextInputFormat.addInputPath(task2Job, new Path(task2InputString));    	
    	
		task2Job.setMapperClass(Top50Mapper1.class);
		task2Job.setMapOutputKeyClass(IntTextPair1.class);
		task2Job.setMapOutputValueClass(Text.class);
		
		task2Job.setGroupingComparatorClass(JoinGroupComparator1.class);
		task2Job.setSortComparatorClass(ValueSortComparator.class);
		//task2Job.setPartitionerClass(Task2Partitioner.class);
 	   	
		task2Job.setReducerClass(Top50Reducer.class);
		task2Job.setNumReduceTasks(6);
 	   	
    	
    	task2Job.setOutputKeyClass(Text.class);
		task2Job.setOutputValueClass(Text.class);
		FileOutputFormat.setOutputPath(task2Job, task2OutPath);
		
		*//////////////////////////////////
    	
    }
}
