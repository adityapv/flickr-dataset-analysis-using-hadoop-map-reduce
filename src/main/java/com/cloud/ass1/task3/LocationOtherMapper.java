package com.cloud.ass1.task3;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.charset.CharsetEncoder;

import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Mapper.Context;

public class LocationOtherMapper extends Mapper<Object, Text, Text, Text> {
	
public void map(Object key, Text value, Context countLocality) throws IOException, InterruptedException{
		
		Text locality = new Text();
		Text details = new Text();
		String tagString = new String();
		String dateString = new String();
		//StringBuffer tagDetails = new StringBuffer();
		
		String[] preTaskPlaceArray = value.toString().split("\t");
		String localityString = preTaskPlaceArray[2];
		String placeName = preTaskPlaceArray[3];
		
		//String localityString = localityArray[0];
		
		String[] preTaskPhotoArray = value.toString().split("ptag");
		String val = value.toString();
//		System.out.println("\n"+"\n"+val+"\n"+"\n");
		for(int i = 1; i < preTaskPhotoArray.length; i++){
//			System.out.println("\n"+"IN FOR"+"\n"+preTaskPhotoArray[i]+"\n"+"\n");
			String[] photoString = preTaskPhotoArray[i].split("\t");
			
			tagString =  photoString[2];
	//		System.out.println(tagString+"\n"+"\n");
			dateString = photoString[3];
		//	System.out.println(dateString+"\n"+"\n");
			
			StringBuffer tagDetails = new StringBuffer();
			tagDetails.append(tagString+"\t"+dateString+"\t"+placeName);

			
			details.set(tagDetails.toString());
			locality.set(localityString);
			
			
			countLocality.write(locality, details);
		}
		
		
				
	}

}
