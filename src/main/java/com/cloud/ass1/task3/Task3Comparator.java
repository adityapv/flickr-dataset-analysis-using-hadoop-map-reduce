package com.cloud.ass1.task3;


import org.apache.hadoop.io.WritableComparable;
import org.apache.hadoop.io.WritableComparator;

/**
 * Group comparator decides which key/value pairs are grouped as a list
 * for a single reduce function
 * @author Ying Zhou
 *
 */

public class Task3Comparator extends WritableComparator{
	
	static int count = 0;
	protected Task3Comparator() {
		super(Task3Pair.class,true);
		// TODO Auto-generated constructor stub
	}

	/**
	 * Only compare the key when grouping reducer input together
	 */
	public int compare(WritableComparable w1, WritableComparable w2) {
		Task3Pair tip1 = (Task3Pair) w1;
		Task3Pair tip2 = (Task3Pair) w2;
		
		if ((tip1.getKey().compareTo(tip2.getKey()) == 0)){
			count++;
		return tip1.getValue().compareTo(tip2.getValue());
		
		}
		
		else{
		count = 0;
		return tip1.getKey().compareTo(tip2.getKey());
		}
	}

}
