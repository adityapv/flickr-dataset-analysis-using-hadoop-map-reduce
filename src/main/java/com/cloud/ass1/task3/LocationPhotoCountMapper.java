package com.cloud.ass1.task3;

import java.io.IOException;

import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Mapper.Context;

import com.cloud.ass1.comparators.IntTextPair;

public class LocationPhotoCountMapper extends Mapper<Object, Text, Text, Text>{


	public void map(Object key, Text value, Context countLocality) throws IOException, InterruptedException{
		
		Text locality = new Text();
		Text photoCount = new Text();
		
		String[] task2DataArray = value.toString().split("\t");
		String task = value.toString();		
		if(task2DataArray.length < 2) return;
		
		//System.out.println("\n"+"\n"+task+"\n");
		//int photoCountInt = Integer.parseInt(task1DataArray[1]);
		//System.out.println(" "+photoCountInt+"\n");
		
		StringBuffer buf = new StringBuffer();
				buf.append(task2DataArray[1]);
		
		photoCount.set(buf.toString());
		locality.set(task2DataArray[0]);
		
		
		countLocality.write(locality, photoCount);
				
	}
}
