package com.cloud.ass1.task3;


import java.io.IOException;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

import com.cloud.ass1.comparators.IntTextPair;

public class TagCountCombiner extends Reducer<Text, Text, Text, Text>{

	int i = 0;
	   
	public void reduce(Text key, Iterable<Text> values, Context tagSortContext) throws IOException, InterruptedException {
		
		Text locality = new Text(), tag= new Text();
		

		
		String[] localityString = key.toString().split("\t");
		

		String tagString = new String();
		StringBuffer buf = new StringBuffer();
		int tagCount = 0;
		
		String localityCountSet = localityString[0].replace("\\count", "\t");
		
		for (Text t : values){
			//System.out.println("\n"+"\n"+"\n"+"\n"+key.toString()+"\t"+t.toString()+"\n"+"\n"+"\n"+"\n");			
	
		tagCount++;	
			
		}
		
		buf.append(localityString[1]+":"+String.valueOf(tagCount));
		
		tagSortContext.write(new Text(localityCountSet), new Text(buf.toString()));
		
		
		
	
		
	}
	
	
	
}


