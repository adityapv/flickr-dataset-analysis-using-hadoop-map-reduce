package com.cloud.ass1.task3;



import org.apache.hadoop.mapred.JobConf;
import org.apache.hadoop.mapreduce.Partitioner;

import com.cloud.ass1.comparators.IntTextPair;

import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.Text;

/**
 * Partition basaed on Key, ignore the int
 * @author Ying Zhou
 *
 */

public class Task3Partitioner extends Partitioner<Task3Pair, Text> {

	@Override
	public int getPartition(Task3Pair key, Text value, int numPartition) {
		// TODO Auto-generated method stub
		return (key.getKey().hashCode() & Integer.MAX_VALUE) % numPartition;
	}
		
}

