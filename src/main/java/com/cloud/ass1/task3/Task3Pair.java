package com.cloud.ass1.task3;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.io.WritableComparable;

/**
 * A composite key used to perform Join
 * @author Ying Zhou
 *
 */
public class Task3Pair implements WritableComparable<Task3Pair>{

	private Text key;
	private Text value;
	private StringBuffer values = new StringBuffer();
	private String[] valueSort;
	int  keyCount = 0;
	
	public String getKey() {
		return key.toString();
	}

	public void setKey(Text key) {
		this.key = key;
	}

	public String getValue() {
		return value.toString();
	}

	public void setValue(Text value) {
		//values.append(value.toString()+",");
		//this.value = new Text(values.toString());
		this.value = value;
	}	
	
	public Task3Pair(){
		this.key = new Text();
		this.value = new Text();
	}
	
	public Task3Pair(Text key, Text value){
		this.key = new Text(key);
		this.value = new Text(value);
	}
	
	public void readFields(DataInput in) throws IOException {
		// TODO Auto-generated method stub
		key.readFields(in);
		value.readFields(in);
		
	}

	@Override
	public void write(DataOutput out) throws IOException {
		// TODO Auto-generated method stub
		//String outString = out.toString();
		//System.out.println("\n"+"\n"+"\n"+"IN COMPOSITE-----------------"+outString+"\n"+"\n"+"\n");
		key.write(out);
		value.write(out);
		//System.out.println("\n"+"IN DATAOUTPUT-----------------"+value.toString()+"\n");
		
	}

	@Override
	public String toString() {
		while(keyCount != 50){
			
			keyCount++;
			//System.out.println("\n"+"IN TOSTRING-----------------"+value.toString()+"\n");

			return key.toString() ;
	
		}
		return "";
	}
	
	@Override
	public int compareTo(Task3Pair other) {
		// TODO Auto-generated method stub
		String keyString = key.toString();
		String otherkeyString = other.key.toString();

		int val = keyString.compareTo(otherkeyString);
		
		if ( val == 0 ) return value.toString().compareTo(other.value.toString());

		else{
			
			//int compare = value.compareTo(other.value);
			
			return value.compareTo(other.value);
		
		}
	}

	@Override
	public int hashCode() {
		return key.hashCode() * 163 + value.hashCode();
	}

	public boolean equals(Object other) {
		if (other instanceof Task3Pair) {
			Task3Pair tip = (Task3Pair) other;
			return key.equals(tip.key) && value.equals(tip.value);
		}
		return false;
	}
}
