package com.cloud.ass1.task3;


import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.charset.CharsetEncoder;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;



public class TagCountMapper extends Mapper<Object, Text, Text, Text> {
	
	public void map(Object key, Text value, Context countLocality) throws IOException, InterruptedException{
		
		String[] valueString = value.toString().split("\t"); 
		Text localityString = new Text();
		Text tag = new Text();
		StringBuffer buf = new StringBuffer();
		
		buf.append(valueString[0]+"\t"+valueString[1]);
		tag.set(valueString[1]);
	
System.out.println("\n"+"\n"+"\n"+"\n"+valueString[0]+"\t"+valueString[1]+"\n"+"\n"+"\n"+"\n");			
		countLocality.write(new Text(buf.toString()), tag);
		
				
	}

}


