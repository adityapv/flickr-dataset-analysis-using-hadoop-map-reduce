package com.cloud.ass1.preTask;

import java.io.IOException;

import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;



public class FileJoinReducer extends Reducer<Text, Text, Text, Text> {

	Text placeId = new Text(), jointRowDetails = new Text();
	
	public void reduce (Text key, Iterable<Text> values, Context localityPhotoContext) throws IOException, InterruptedException {

		String locality = "";
		StringBuffer jointRowBfr = new StringBuffer();
		for (Text text: values){
			
			String[] textArray = text.toString().split("\t"); 
			//System.out.println(key.toString()+"\n"+text.toString()+"\n"+"\n");
			
			
			
			if (textArray[0].equals("ltag")){
				
				locality = "ltag\t"+textArray[1]+"\t"+textArray[2]+",";
				//System.out.println("in LOCALITY");
				//System.out.println(locality);
			}
			
			if (textArray[0].equals("ptag")) {
				
				
				jointRowBfr.append("ptag\t"+textArray[1]+"\t"+textArray[2]+"\t"+textArray[3]+",");
				//System.out.println("in PHOTO");
				//System.out.println(locality+"\t"+jointRowBfr.toString());
				//jointRowBfr.append(text.toString());
				
							
			}
			
		
		}
		jointRowDetails.set(locality+"\t"+jointRowBfr.toString());
		if (locality.equals("")) return;
		localityPhotoContext.write(key, jointRowDetails);

		

	}
}

