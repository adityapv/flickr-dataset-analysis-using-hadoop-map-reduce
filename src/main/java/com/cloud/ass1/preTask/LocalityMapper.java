package com.cloud.ass1.preTask;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.charset.CharsetEncoder;

import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

public class LocalityMapper extends Mapper<Object, Text, Text, Text>{
	
	Text placeId = new Text(), locality = new Text();
	static CharsetEncoder asciiEncoder = Charset.forName("US-ASCII").newEncoder();
	
	public void map (Object key, Text value, Context placeIdLocalityContext) throws IOException, InterruptedException {
		
		String[] placeArray = value.toString().split("\t");
		
		if (placeArray.length < 6) return;
		
		String placeIdString = placeArray[0];
		//int placeTypeId = Integer.parseInt(placeArray[5]);
		String placeName = placeArray[4];
		
		String localityUrlString = placeArray[6];
		//System.out.println("\n"+"\n"+"\n"+"\n"+localityUrlString+"\t"+placeIdString+"\t"+placeArray[5]);
		String localityString = new String();
		
		if (placeIdString.length() > 0 && asciiEncoder.canEncode(placeIdString)) {
			
			placeId.set(placeIdString);
			
		}
		
		if ((localityUrlString.length() > 0 && asciiEncoder.canEncode(localityUrlString)) && asciiEncoder.canEncode(localityUrlString)) {
			
			if (placeArray[5].equals("7") ) {
				String[] localityUrlArray = localityUrlString.split("/");
				// new logic
				StringBuffer bfr = new StringBuffer();
				int count = localityUrlArray.length-1;
				for (int i = count; i > 0; i--){
					bfr.append(localityUrlArray[i]+",");
				}
				localityString = bfr.toString();
				
				//System.out.println("\n"+"\n"+"\n"+"\n"+localityString+"\t"+placeIdString);
				locality.set("ltag\t"+localityString+"\t"+placeName);
			}
			
			else if (placeArray[5].equals("22") ) {
				String[] localityUrlArray = localityUrlString.split("/");
				//new logic
				StringBuffer bfr = new StringBuffer();
				int count = localityUrlArray.length-2;
				for (int i = count; i > 0; i--){
					bfr.append(localityUrlArray[i]+",");
				}
				localityString = bfr.toString();
				
				//localityString = localityUrlArray[localityUrlArray.length-2];
				//System.out.println("\n"+"\n"+"\n"+"\n"+localityString+"\t"+placeIdString);
				locality.set("ltag\t"+localityString+"\t"+placeName);
			}
			
			else { return;}
			
			//System.out.println("\n"+"\n"+"\n"+"\n"+locality.toString()+"\t"+placeIdString);
			placeIdLocalityContext.write(placeId, locality);

		}
		
	}
	

}
