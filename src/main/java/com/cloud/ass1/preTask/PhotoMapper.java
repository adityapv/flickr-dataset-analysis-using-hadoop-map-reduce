package com.cloud.ass1.preTask;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.charset.CharsetEncoder;

import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;


public class PhotoMapper extends Mapper<Object, Text, Text, Text>{
	
	Text photoDetails = new Text(), placeId = new Text();

	static CharsetEncoder asciiEncoder = Charset.forName("US-ASCII").newEncoder();
	
	public void map (Object key, Text value, Context photoContext) throws IOException, InterruptedException {
		
		String[] valueArray = value.toString().split("\t");
		
		if ( valueArray.length < 6) return;
		
		String placeIdString = valueArray[4]; 
		String photoIdtring = valueArray[0];
		String tagString = valueArray[2];
		String dateString = valueArray[3];
		
		StringBuffer photoDetailBfr = new StringBuffer();
		photoDetailBfr.append(photoIdtring+"\t"+tagString+"\t"+dateString+"\t");
		
		if (photoDetailBfr.length() > 0 && asciiEncoder.canEncode(photoDetailBfr)) {
			
			photoDetails.set("ptag\t"+photoDetailBfr.toString());
		}
		
		if (placeIdString.length() > 0 ) {
		
			placeId.set(placeIdString);
		}
		
		if ( asciiEncoder.canEncode(tagString)){
		photoContext.write(placeId, photoDetails);
		}
		
		else return;
		
	}	
}
