package com.cloud.ass1.task2;

import org.apache.hadoop.mapred.JobConf;
import org.apache.hadoop.mapreduce.Partitioner;

import com.cloud.ass1.comparators.IntTextPair;

import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.Text;

/**
 * Partition basaed on Key, ignore the int
 * @author Ying Zhou
 *
 */

public class Task2Partitioner extends Partitioner<IntTextPair, NullWritable> {

	@Override
	public int getPartition(IntTextPair key, NullWritable value, int numPartition) {
		// TODO Auto-generated method stub
		return (key.getKey().hashCode() & Integer.MAX_VALUE) % numPartition;
	}
		
}