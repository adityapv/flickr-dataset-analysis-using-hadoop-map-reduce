package com.cloud.ass1.task2;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.charset.CharsetEncoder;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

import com.cloud.ass1.comparators.IntTextPair;

public class Top50Mapper extends Mapper<Object, Text, IntTextPair, NullWritable> {
	
	public void map(Object key, Text value, Context countLocality) throws IOException, InterruptedException{
		
		Text locality = new Text();
		Text photoCount = new Text();
		
		
		String[] task1DataArray = value.toString().split("\t");
		//System.out.println(task1DataArray[1]+" "+task1DataArray[0]+"\n");
		int photoCountInt = Integer.parseInt(task1DataArray[1]);
		//System.out.println(" "+photoCountInt+"\n");
		photoCount.set(task1DataArray[1]);
		locality.set(task1DataArray[0]);
		
		
		countLocality.write(new IntTextPair(photoCount, locality), NullWritable.get());
				
	}

}
