package com.cloud.ass1.comparators;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

import org.apache.hadoop.io.Text;
import org.apache.hadoop.io.WritableComparable;

public class IntTextPair1 implements WritableComparable<IntTextPair1>{
	
	private Text key;
	
	public Text getKey() {
		return key;
	}

	public void setKey(Text key) {
		this.key = key;
	}

	public IntTextPair1(){
		this.key = new Text();
	}
	
	public IntTextPair1(Text key){
		this.key = new Text(key);
	}
	
	public void readFields(DataInput in) throws IOException {
		// TODO Auto-generated method stub
		key.readFields(in);
	}
	
	@Override
	public void write(DataOutput out) throws IOException {
		key.write(out);
	}
	
	@Override
	public int compareTo(IntTextPair1 other) {
		// TODO Auto-generated method stub
		String keyString = key.toString();
		int keyInt = Integer.parseInt(keyString);
		
		String otherkeyString = other.key.toString();
		int otherkeyInt = Integer.parseInt(otherkeyString);		
		if (keyInt < otherkeyInt) return 1;
		if (keyInt > otherkeyInt) return -1;
		else return 0;
	}
	
	public boolean equals(Object other) {
		if (other instanceof IntTextPair1) {
			IntTextPair1 tip = (IntTextPair1) other;
			return key.equals(tip.key) ;
		}
		return false;
	}

}
