package com.cloud.ass1.comparators;

import org.apache.hadoop.io.WritableComparable;
import org.apache.hadoop.io.WritableComparator;

/**
 * Group comparator decides which key/value pairs are grouped as a list
 * for a single reduce function
 * @author Ying Zhou
 *
 */
public class JoinGroupComparator extends WritableComparator {

	protected JoinGroupComparator() {
		super(IntTextPair.class,true);
		// TODO Auto-generated constructor stub
	}

	/**
	 * Only compare the key when grouping reducer input together
	 */
	public int compare(WritableComparable w1, WritableComparable w2) {
		IntTextPair tip1 = (IntTextPair) w1;
		IntTextPair tip2 = (IntTextPair) w2;

		return tip1.getKey().compareTo(tip2.getKey());
	}
	
}