package com.cloud.ass1.comparators;

import org.apache.hadoop.io.Text;
import org.apache.hadoop.io.WritableComparable;
import org.apache.hadoop.io.WritableComparator;

public class ValueSortComparator extends WritableComparator  {
	
	public int compare(WritableComparable w1, WritableComparable w2) {
		Text tip1 = (Text) w1;
		Text tip2 = (Text) w2;
		return tip1.toString().compareTo(tip2.toString());
	}

}
